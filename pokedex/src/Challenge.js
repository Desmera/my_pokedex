import React from "react";
import {useState} from "react";
import "./Styles/Styles.scss";
import challengesAndAuthor from "./Components/ChallengesAndAuthor"
import 'bootstrap/dist/css/bootstrap.css';


export default function Chalenge () {

    const [challenge, setChallenge] = useState (challengesAndAuthor[0]);

        const handleClick = () => {
            let num = Math.floor(Math.random() * challengesAndAuthor.length)
            let newChallenge = challengesAndAuthor[num];

        setChallenge(newChallenge)
           
    };

    
    return(
        <div className="container">

            <div className="container2" >

                <div className="card-body">
            

                    <h1 className="titleChallenge">Are you Ready to Challenge yourself ?</h1>
                    <h5 className="nameChallenge">{challenge.challengeName}</h5>
                    <p className="textChallenge">{challenge.challenge}</p>
                    <h6 className="authorChallenge">{challenge.author}</h6>
            

                    <button onClick={handleClick}>Random Challenge!</button>    
                </div>
            </div>
        </div>
    )
};