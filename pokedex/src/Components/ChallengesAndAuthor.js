const challengesAndAuthor = [
    {
        challengeName: "Monocolor",
        challenge: `This is a unique Pokémon challenge that only allows the players to pick 
        Pokémon of a certain color. For example, if the Trainer picks Blue, their team in Generation 3
        could be Mudkip, Surskit, Minun, Gyarados, Armaldo, and Shiny Shuppet. Yes, shiny Pokémon are allowed.

        The official rules state that players can use a prevolution of a Pokémon even if it does not match the
        color criteria, as long as they evolve it to match the correct color as soon as possible. 
        However, some trainers take this one step further and say that only Pokémon that exactly fit the color 
        determine whether or not a Pokemon can evolve.`,
        author: "Sara Haritage",
    },

    {
        challengeName:"Monotype",
        challenge: `An evolution of the Single-Pokémon runs, a Monotype challenge is a playthrough that puts
        players in the shoes of their favorite gym leader. In a Monotype challenge, trainers must restrict their
        party of six to a single type. This adds an extra layer of difficulty when a Fire-type Monotype challenger
        is faced with a Water gym, or when a Bug-type Monotype team faces down against Flying-type Pokémon. 
        However, it does introduce some creativity with TMs and stats that most trainers don’t get to work with otherwise.

        Some might be thinking they can circumvent the rules by adding dual-types to their teams. However, a common add-on
        to the rules dictates that only monotype Pokémon can be used. This proves incredibly difficult in earlier games,
        such as Pokémon Red and Blue, where there are only three Dragon-type Pokémon.`,
        author: "Sara Haritage",
    },

    {
        challengeName:"Single Pokémon Runs",
        challenge: `Every trainer has their favorite Pokémon. It could be their first-ever starter Pokémon, 
        or just one they feel is criminally underrated. No matter what, there’s a place for every Pokémon 
        in this challenge mode.
        
        The tricky part of this mode comes from the fact that trainers cannot use another Pokémon, ever.
        A lot of players, such as YouTuber and streamer Mah-Dry-Bread, stipulate that other Pokémon can
        be used for HMs outside of battle, but cannot be used in battle. If this was not the case, 
        generations 1-6 could not be completed. This is a great run that often shines a light on Pokémon players
        might never have considered using before.`,
        author: "Sara Haritage",
    },

    {
        challengeName:"No New Moves",
        challenge: `This challenge is a brutal one. A No New Moves challenge run typically does not put restrictions
        on the number of Pokémon a Trainer can catch, but it is exactly what it sounds like. Players can only use the 
        moves that Pokémon comes with. This is especially brutal in the early game. In early routes, most Pokémon only
        know Normal-type Physical moves such as Scratch and/or Tackle.
        
        A lot of challengers also stipulate that TMs and, in generations prior to Generation 7, HMs are not allowed. 
        This leads to creative workarounds such as out-of-bounds glitches and exploits. It also leads to an immense 
        amount of strategy and planning prior to starting the challenge. Trainers will have to work out what level 
        to catch wild Pokémon at to obtain the correct moves and which route is best to find these Pokémon. 
        That's not even mentioning breeding Pokémon for specific egg moves, which adds a whole new layer of difficulty to this run.`,
        author: "Sara Haritage",
    },

    {
        challengeName:"No Damage",
        challenge: `Popularised by streamer and YouTuber SmallAnt, the ‘No Damage’ Pokémon challenge is only 
        for those who are willing to sink in hours and hours of brutal gameplay.

        This legendary feat from SmallAnt was painstaking to watch, and surely even more so to play. The rules were that 
        he could only save after defeating a gym. If he took damage at any point, he would have to reset to this save point.
        By the end of the run, he had reset 69 times in total.
        
        Let's put this ridiculously tough Pokémon challenge into perspective. With Piplup, SmallAnt had a 0.004% chance of beating 
        their first rival battle. He needed his enemy to withdraw six times in a row, as well as miss their attack twice. 
        This would require silly amounts of luck, so he decided to reset and choose Turtwig as his starter. This meant that his rival
        would choose the Fire-type Chimchar. His rival would then need to use Leer four times in a row, which has a 6.25% chance of occurring.
        This introductory fight took nearly an hour, but the run was only getting started. The next fight, a level 5 Starly with Quick Attack,
        took nine hours of grinding to win.`,
        author: "Sara Haritage",
    },

    {
        challengeName:"Randomiser",
        challenge: `While randomizing a Pokemon game makes for an interesting experience on its own, this is actually a fantastic modifier 
        to attach to any other challenge. Doing this takes a bit of know-how, as to do it to an official game requires either Homebrewing 
        a console or installing an Emulator. However, several fan games have this as a built-in option. The craziness of encountering a Mewtwo
        as a Starter or the champion's signature Pokemon replaced with a Magikarp is always a blast.`,
        author: "Ryan Woodrow",
    },

    {
        challengeName:"Egglocke",
        challenge: `One of the more fiddly ones to set up, but Egglockes can make for unique playthroughs. Generating eggs is fine enough, 
        but getting them from friends or online communities is a lot more fun due to the complete surprise of what each egg contains. 
        Some will no doubt be trolls that give garbage Pokemon, while others will hold something amazing. It means that every Pokemon 
        will not only have the story of how the player used it but also of who it came from.`,
        author: "Ryan Woodrow",
    },

    {
        challengeName:"Quadlocke",
        challenge: `A Quadlocke expands on the concept of a Monotype run. Rather than being limited to just one type, players will have 
        access to four types over the course of the game. As such, players will have to get the most out of their Pokemon's limited movesets.
        This is great when combined with a randomizer, as it gives players a wider variety in their teams while still giving enough of 
        a restriction to be challenging.`,
        author: "Ryan Woodrow",
    },
]

export default challengesAndAuthor;