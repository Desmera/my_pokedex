import React from "react";
import {useState} from "react";
import "./Styles/Styles.scss";
import Axios from "axios";
import "bootstrap/dist/css/bootstrap.css";

import Container from 'react-bootstrap/Container';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';




const App = () => {
  const [pokemonName, setPokemonName] = useState("");

  const [pokemonChosen, setPokemonChosen] = useState(false);

  const [pokemon, setPokemon] = useState({
    name: "",
    number: "",
    species: "",
    image: "",
    hp: "",
    attack: "",
    defense: "",
    speed: "",
    type: "",
    });

    const searchPokemon = () => {
      Axios.get(`https://pokeapi.co/api/v2/pokemon/${pokemonName}`).then(
        (res) => {
          setPokemon({
            name: pokemonName,
            number: res.data.id,
            species: res.data.species.name,
            image: res.data.sprites.front_default,
            hp: res.data.stats[0].base_stat,
            attack: res.data.stats[1].base_stat,
            defense: res.data.stats[2].base_stat,
            speed: res.data.stats[5].base_stat,
            type: res.data.types[0].type.name,
          });
          setPokemonChosen(true);
          }
        );
      };

    
  return (

    
    <div className="App">              
       

        <Container>
          <Row>
            <Col>
              <div className="TitleSection">

                
                    <h1>Find your Pokemon!</h1>
                

                <input
                  type="text"
                  onChange={(event) => {
                    setPokemonName(event.target.value);
                  } }
                  value={pokemonName.toLowerCase()} />

                <div>
                  {pokemonName && <button onClick={searchPokemon}>Search Pokémon</button>}
                </div>

              </div>



              <div className="DisplaySection">
                {!pokemonChosen ? (
                  <p> Curiosity: It may be entry #112 in the Pokédex, 
                  but according to Ken Sugimori, Rhydon was the first 
                  Pokémon ever created. </p>
                  
                ) : (
                  <>
                  <div className="pokeInfo">
                    <h1>{pokemon.name}</h1>
                    <img src={pokemon.image} alt={pokemon.name} />
                    <h3>Number: #{pokemon.number}</h3>
                    <h5>Species: {pokemon.species}</h5>
                    <h5>Type: {pokemon.type}</h5>
                    <h5>Hp: {pokemon.hp}</h5>
                    <h5>Attack: {pokemon.attack}</h5>
                    <h5>Defense: {pokemon.defense}</h5>
                    <h5>Speed: {pokemon.speed}</h5>
                  </div>
                  </>
                )}
              </div>
            </Col>
          </Row>
        </Container>
      </div>
  );
};

export default App;