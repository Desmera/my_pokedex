import React from "react";
import {BrowserRouter, Route, Switch} from "react-router-dom";
import "bootstrap/dist/css/bootstrap.css";
import Home from "./Home";
import Challenge from "./Challenge";
import { Navbar, Nav, Container } from 'react-bootstrap';


export default function App () {

  return (

    
    <div className="App">

        <BrowserRouter>
        <div className="navContainer">
          <Navbar className="Nav">
              <Container>
                <Nav className="me-auto navbar">
                    <Nav.Link href= "/">Home</Nav.Link>
                    <Nav.Link  href="/Challenge">Challenges</Nav.Link>
                </Nav>
              </Container>
          </Navbar>
        </div>

          <Switch>

            <Route exact path="/">
                  <Home />
            </Route>

            <Route path="/Challenge">
                  <Challenge />
            </Route>

          </Switch>
        </BrowserRouter>


                  
       

      </div>
  );
};


